setlocal complete+=t
setlocal formatoptions-=t
if v:version > 702 && !&relativenumber
    setlocal number
endif
setlocal nowrap
setlocal textwidth=79
setlocal colorcolumn=100,120
setlocal commentstring=#%s
setlocal define=^\s*\\(def\\\\|class\\)

