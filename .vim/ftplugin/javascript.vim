let b:ale_linters = ['eslint', 'flow', 'flow-language-server', 'jscs', 'jshint', 'standard', 'xo']

setlocal textwidth=79
setlocal colorcolumn=100,120
