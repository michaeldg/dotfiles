## Permissions

### Make file permissions of group same as owner

    sudo chmod -R g=u .


## Video

### Convert mkv to mp4:

    ffmpeg -i input.mkv -codec copy output.mp4


### Extract subtitles from mkv:

You need this tool:

    sudo apt-get install mkvtoolnix

Then you can use either of these commands to get information about the
different subtitle tracks:

    mkvinfo movie.mkv
    mkvmerge -i movie.mkv

Then use this command to extract the desired subtitle track:

    mkvextract tracks movie.mkv 1:subtitles.srt

In this example, subtitle track 1 is extracted, replace this number with the
desired subtitle track.


## Audio

### Reinstall ALSA and PulseAudio

    sudo apt-get remove alsa-base pulseaudio
    sudo apt-get install alsa-base pulseaudio
    sudo alsa force-reload

### Reset PulseAudio settings

    mv ~/.config/pulse ~/.config/old_pulse


## Download music

### Soundcloud

Soundcloud Music Downloader - https://github.com/flyingrub/scdl

    scdl -l <song-url>


### Youtube

youtube-dl - https://youtube-dl.org/

An alias for easy downloading of only the audio track:

    alias youtube-dl-audio='youtube-dl --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format mp3'

Then just use

    youtube-dl-audio <youtube-video-url>

More info: https://askubuntu.com/a/1199994/230971
