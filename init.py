#!/usr/bin/env python3
import os
import sys
from shutil import which
from sysconfig import get_makefile_filename


required_bins = [
    'cmake',
    'curl',
    'diff-so-fancy',
    'git',
    'powerline',
    'wget',
    ('lsd', 'download lsd at https://github.com/Peltoche/lsd#installation'),
    ('ctags', 'execute `apt install exuberant-ctags`'),
    ('exa', 'download exa at https://the.exa.website/ and place the binary in /opt/'),
    ('ctags', 'execute `apt install exuberant-ctags`'),
    ('pip', 'execute `apt install python-pip`'),
]

config_files = [
    '.bashrc',
    '.config/flake8',
    '.config/kitty/kitty.conf',
    '.config/kitty/open-actions.conf',
    '.config/kitty/themes/',
    '.config/nvim/init.vim',
    '.config/ptpython/config.py',
    '.config/terminator/config',
    '.gitconfig',
    '.gitignore',
    '.my.cnf',
    '.nvm/',
    '.oh-my-zsh/custom/themes/gitaarik.zsh-theme',
    '.oh-my-zsh/custom/themes/powerlevel10k/',
    '.p10k.zsh',
    '.pdbrc',
    '.pylintrc',
    '.screenrc',
    '.sh_plugins/',
    '.ssh/config',
    '.tmux.conf',
    '.vim/',
    '.vimrc',
    '.z.sh/',
    '.zshrc',
]


def update_git_repo():
    os.system('git submodule update --init --recursive')


def check_requirements():

    errors = []

    for required_bin in required_bins:

        if isinstance(required_bin, tuple):
            required_bin, install_instructions = required_bin
        else:
            install_instructions = None

        if not which(required_bin):

            def install_instructions_message():
                if install_instructions:
                    return "(To install: {})".format(install_instructions)
                return ''

            errors.append("'{}' not installed. {}".format(required_bin, install_instructions_message()))

    if not get_makefile_filename():
        errors.append("Python development files not installed. On Ubuntu: sudo apt install python-dev python3-dev")

    if errors:
        for error in errors:
            print("Errors:")
            print(error)
            sys.exit()


def homedir(path):
    return os.path.join(os.path.expanduser('~'), path)


def link_config_files():

    for config_file in config_files:

        if '/' in config_file and config_file[-1] != '/':
            dirname = config_file.rsplit('/', 1)[0]
            os.makedirs(homedir(dirname), exist_ok=True)

        if config_file[-1] == '/':
            path = config_file[0:-1]
        else:
            path = config_file

        config_path = homedir(path)

        if os.path.exists(config_path):

            if os.path.islink(config_path):
                os.remove(config_path)
            else:

                base_backup_path = '{}.backup'.format(config_path)
                backup_path = base_backup_path
                backup_path_num = 0

                # If an old backup already exists, then move this old backup to the
                # Trash.
                while os.path.exists(backup_path):
                    backup_path_num += 1
                    backup_path = '.'.join([base_backup_path, str(backup_path_num)])

                try:
                    os.rename(config_path, backup_path)
                except FileNotFoundError:
                    pass
                else:
                    print("Created backup at: {}".format(backup_path))

        os.symlink(os.path.abspath(path), config_path)
        print(f"Linked config file {config_path}")


update_git_repo()
check_requirements()
link_config_files()
