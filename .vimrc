let mapleader = "\<Space>"

call plug#begin('~/.vim/plugged')


    " PaperColor colorscheme
    " ----------------
    " Clear colorscheme for dark and light backrounds
    "
    " https://github.com/nlknguyen/papercolor-theme

        Plug 'nlknguyen/papercolor-theme'


    " Vim Polyglot
    " ------------
    " Syntax highlighting and indentation support for multiple languages
    "
    " https://github.com/sheerun/vim-polyglot

        Plug 'sheerun/vim-polyglot'


    " Syntax highlighting plugins
    " --------------
    " Jinja2 syntax highlighting, not present in Vim Polyglot
    "
    " https://github.com/glench/vim-jinja2-syntax

        Plug 'glench/vim-jinja2-syntax'


    " vim-airline
    " -----------
    " Nice status line at bottom
    "
    " https://github.com/vim-airline/vim-airline
    " https://github.com/vim-airline/vim-airline-themes#vim-airline-themes--
    " https://github.com/ajmwagar/vim-deus

        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'
        Plug 'ajmwagar/vim-deus'

        let g:airline_theme='deus'
        let g:airline_powerline_fonts = 1 " Use powerline fonts
        let g:airline#extensions#tabline#enabled = 1 " Enable tabline
        let g:airline#extensions#tabline#show_tabs = 1 " Show tabs in the tabline
        let g:airline#extensions#tabline#show_buffers = 0 " Don't show buffers in the tabline
        let g:airline#extensions#tabline#show_splits = 0 " Don't show splits in the tabline
        let g:airline#extensions#tabline#tab_min_count = 2 " Require at least 2 tabs to show the tabline
        let g:airline#extensions#tabline#tabs_label = '' " Remove the 'tabs' text at the start of the tabline
        let g:airline#extensions#tabline#fnamemod = ':t' " Only show the filename in the tab, not the path


    " Vim transparent
    " ---------------
    " Remove all background color on Vim
    "
    " https://github.com/tribela/vim-transparent

        Plug 'tribela/vim-transparent'


    " tmuxline.vim
    " ------------
    " Vim-airline integration in tmux
    "
    " https://github.com/edkolev/tmuxline.vim

        Plug 'edkolev/tmuxline.vim'


    " Signify
    " -------------
    " Show a git diff using Vim's sign column
    "
    " https://github.com/mhinz/vim-signify

        if has('nvim') || has('patch-8.0.902')
            Plug 'mhinz/vim-signify'
        else
            Plug 'mhinz/vim-signify', { 'branch': 'legacy' }
        endif

        " Nice looking status line
        " https://github.com/Optixal/neovim-init.vim/blob/master/init.vim#L270-L274
        let g:signify_sign_add = '│'
        let g:signify_sign_delete = '|'
        let g:signify_sign_change = '│'
        hi DiffDelete guifg=#ff5555 guibg=none


    " fugitive.vim
    " ------------
    " Git tools inside Vim
    "
    " https://github.com/tpope/vim-fugitive

        Plug 'tpope/vim-fugitive'

        " Alt + b for git blame
        nnoremap <silent> <M-b> :Git blame<CR>


    " Auto Pairs
    " ----------
    " Automatically add closing quotes/brackets as you type
    "
    " https://github.com/jiangmiao/auto-pairs
    "

        "Plug 'jiangmiao/auto-pairs'

        "" Disable the toggle and fast wrap shortcuts, because they conflict with
        "" existing shortcuts.
        "" https://github.com/jiangmiao/auto-pairs#shortcuts
        "let g:AutoPairsShortcutToggle = ''
        "let g:AutoPairsShortcutFastWrap = ''


    " surround.vim
    " ------------
    " Shortcuts for handing strings within quotes etc.
    "
    " https://github.com/tpope/vim-surround

        Plug 'tpope/vim-surround'


    " vim match-up
    " ------------
    " Better jumping between start and end tags with `%`
    "
    " https://github.com/andymass/vim-matchup

        Plug 'andymass/vim-matchup'


    " Vim Noh
    " -------
    " Automatically clear search highlighting when cursor is moved
    "
    " https://github.com/jesseleite/vim-noh

        Plug 'jesseleite/vim-noh'


    " NERD Commenter
    " --------------
    " For easily commenting out big pieces of code
    "
    " https://github.com/preservim/nerdcommenter

        Plug 'preservim/nerdcommenter'


    " vim-visual-multi
    " ----------------
    " Sublime-like multi cursors
    "
    " https://github.com/mg979/vim-visual-multi

        Plug 'mg979/vim-visual-multi', {'branch': 'master'}


    " vim-highlightedyank
    " -------------------
    " Highlight the text that was yanked.
    "
    " https://github.com/machakann/vim-highlightedyank

        Plug 'machakann/vim-highlightedyank'


    " NERDTree
    " --------
    " File navigation side-bar
    "
    " https://github.com/preservim/nerdtree

        Plug 'preservim/nerdtree'
        let NERDTreeIgnore = ['\.pyc$', '__pycache__', 'node_modules', 'tags']

        nnoremap <silent> <leader>r :NERDTreeToggle<CR>
        nnoremap <silent> <leader>f :NERDTreeFind<CR>

        " Open the existing NERDTree on each new tab.
        autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif

    " nerdtree-git-plugin
    " -------------------
    " Show git status flags in NERDTree
    "
    " https://github.com/Xuyuanp/nerdtree-git-plugin

        Plug 'Xuyuanp/nerdtree-git-plugin'


    " VimDevIcons
    " -----------
    " Adds file type icons to other Vim plugins, like NERDTree
    "
    " https://github.com/ryanoasis/vim-devicons

        " NOTE: This should be BELOW nerdtree-git-plugin, see FAQ of
        " nerdtree-git-plugin:
        " https://github.com/Xuyuanp/nerdtree-git-plugin
        " Plug 'ryanoasis/vim-devicons'


    " vim-nerdtree-syntax-highlight
    " -----------------------------
    " Adds syntax for nerdtree on most common file extensions.
    "
    " https://github.com/tiagofumo/vim-nerdtree-syntax-highlight

        Plug 'tiagofumo/vim-nerdtree-syntax-highlight'


    " FZF
    " ---
    " Fuzzy search file finder
    "
    " https://github.com/junegunn/fzf.vim
    " https://github.com/pbogut/fzf-mru.vim

        Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        Plug 'junegunn/fzf.vim'
        Plug 'yuki-yano/fzf-preview.vim', { 'branch': 'release/rpc' }

        let git_root = system('git rev-parse --show-toplevel 2>/dev/null')

        " Ctrl + p for most recently used files
        " Alt + p for all files
        " When in a git project, use `FzfPreviewProject*`, otherwise use
        " `FzfPreview*`.
        if git_root ==# ''
            nnoremap <silent> <C-p> :FzfPreviewMruFilesRpc<CR>
            nnoremap <silent> <M-p> :FzfPreviewDirectoryFilesRpc<CR>
        else
            nnoremap <silent> <C-p> :FzfPreviewProjectMruFilesRpc<CR>
            nnoremap <silent> <M-p> :FzfPreviewProjectFilesRpc<CR>
        endif

        " Alt + Shift + P for open buffers
        nnoremap <silent> <M-P> :FzfPreviewBuffersRpc<CR>

        " Alt + g for git actions
        nnoremap <silent> <M-g> :CocCommand fzf-preview.GitActions<CR>


    " Gutentags
    " ---------
    " Automatically manage tag files
    "
    " https://github.com/ludovicchabant/vim-gutentags

        Plug 'ludovicchabant/vim-gutentags'


    " Vista
    " -----
    " Tagbar that uses LSP symbols and tags
    "
    " https://github.com/liuchengxu/vista.vim

        Plug 'liuchengxu/vista.vim'
        nnoremap <silent> <leader>t :Vista!!<CR>


    " Tagbar
    " ------
    " Displays tags in a window, ordered by scope
    "
    " https://github.com/preservim/tagbar

        "Plug 'preservim/tagbar'
        "nnoremap <silent> <leader>t :TagbarToggle<CR>


    " coc.nvim
    " --------
    " Nodejs extension host for vim & neovim, load extensions like VSCode and
    " host language servers.
    "
    " https://github.com/neoclide/coc.nvim

        Plug 'neoclide/coc.nvim', {'branch': 'release'}

        let g:coc_global_extensions = ['coc-diagnostic', 'coc-eslint', 'coc-explorer', 'coc-fzf-preview', 'coc-git', 'coc-html', 'coc-htmldjango', 'coc-jedi', 'coc-json', 'coc-lit-html', 'coc-sh']

        Plug 'codechips/coc-svelte', {'do': 'npm install'}


    " jedi-vim
    " --------
    " Python autocompletion
    "
    " https://github.com/deoplete-plugins/deoplete-jedi/
    " https://github.com/davidhalter/jedi-vim

        Plug 'davidhalter/jedi-vim'
        " Plug 'deoplete-plugins/deoplete-jedi'

        " Disable vim-jedi completions, because we use coc.nvim
        " https://github.com/davidhalter/jedi-vim#the-completion-is-too-slow
        let g:jedi#completions_enabled = 0

        let g:jedi#rename_command = "\\r"


    " Pythonsense
    " -----------
    " Text objects and motions for Python
    "
    " https://github.com/jeetsukumaran/vim-pythonsense

        Plug 'jeetsukumaran/vim-pythonsense'


call plug#end()

colorscheme PaperColor

if !has('nvim')
    set nocompatible
endif

set hidden " hide abandoned buffers
set ignorecase " ignore case for search
set smartcase " enable smart case search - will enable case sensitive search when the pattern contains uppercase characters
set cindent " C-like auto indenting
set scrolloff=2 " This ensures you have at least 2 lines of context around your cursor
" set nonumber " don't show numbers before lines
set shiftwidth=4 " set the shift width to 4 spaces
set tabstop=4 " set the tabstops width to 4 spaces
set linebreak " wrap on whole words instead of characters
set softtabstop=4
set expandtab " use spaces instead of tab character
set winminheight=0 " when using splits, this will only show the titlebar of the file and not the title + one line contents of the file
set winminwidth=0 " when using splits, this will only show the vertical split bar and not one column of contents of the splitted file
set nojoinspaces " don't add 2 spaces after joining lines that end with . ! and ?
" set diffopt+=iwhite " ignore whitespaces for vimdiff
set mouse=a " enable mouse support
set undofile " Safe undo's after file closes
set undodir=$HOME/.vim/undo " where to save undo histories
" set backup " keep a backup file
" set backupdir=$HOME/.vim/backups " keep a backup file
set completeopt=menu " for autocomplete suggestions, only use the menu (pops up in insert mode at the cursor location) and not the preview window (which shows up on top of the screen)
set tags=./tags,tags;$HOME " Look for tags files in current file directory, in current working directory and up and up until your $HOME


" Settings to make CoC work niceley:
" ----------------------------------

    " Some servers have issues with backup files, see #649.
    set nobackup
    set nowritebackup

    " The delay before the `CursorHold` event is triggered
    set updatetime=500

    " Don't pass messages to |ins-completion-menu|.
    set shortmess+=c

    " Always show the signcolumn, otherwise it would shift the text each time
    " diagnostics appear/become resolved.
    set signcolumn=yes

    " Use tab for trigger completion with characters ahead and navigate.
    " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    " other plugin before putting this into your config.
    inoremap <silent><expr> <TAB>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<TAB>" :
          \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " Ctrl + k and Ctrl + j for jumping to prev/next diagnostics error
    " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
    nmap <silent> <C-k> <Plug>(coc-diagnostic-prev)
    nmap <silent> <C-j> <Plug>(coc-diagnostic-next)

    " Fix diagnostic of current line
    nmap <silent> <M-f> <Plug>(coc-fix-current)

    " Jump to var definition
    nmap <silent> <M-d> <Plug>(coc-definition)

    " Show var references
    nmap <silent> <M-r> <Plug>(coc-references)

    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call CocActionAsync('highlight')

    " Symbol renaming.
    nmap <leader>rn <Plug>(coc-rename)

" End CoC settings
" ----------------------------------


if !has('nvim')
    set ttymouse=xterm2 " enables mouse dragging inside tmux
endif

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") |
\   exe "normal! g`\"" |
\ endif

" For all text files set 'textwidth' to 80 characters.
autocmd FileType text setlocal textwidth=80

" Folding
set foldmethod=indent
set foldlevel=1
set nofoldenable

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot. Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" Move by visual line instead of whole line
nnoremap j gj
nnoremap k gk

" Keep selection when indenting
vnoremap > >gv
vnoremap < <gv

" Toggle paste mode
set pastetoggle=<M-t>

" Easy tab navigation
nnoremap <silent> <C-l> gt
nnoremap <silent> <C-h> gT

" Easy tab moving
nnoremap <silent> <leader>h :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <leader>l :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" Close tab on space-c
nnoremap <silent> <leader>c :tabc<CR>

nnoremap <silent> <F6> :set bg=dark<CR>
nnoremap <silent> <F8> :set bg=light<CR>

" Easy split navigation
nnoremap <silent> <M-h> <C-w>h
nnoremap <silent> <M-j> <C-w>j
nnoremap <silent> <M-k> <C-w>k
nnoremap <silent> <M-l> <C-w>l

" Jumping 5 lines
nnoremap <silent> <M-[> 5k
nnoremap <silent> <M-]> 5j

" Faster scrolling
nnoremap <silent> <M-e> 5<C-e>
nnoremap <silent> <M-y> 5<C-y>

" use Space-o to insert a new line without going into insert mode
nmap <leader>o m`o<Esc>``

" delete to black hole ("_) so it won't overwrite the last yank
noremap \dd "_dd

" delete with slash deletes to black hole (without yanking)
nnoremap <silent> \d "_d
vnoremap <silent> D "_d
nnoremap <silent> \D "_D

nnoremap <silent> \x "_x
vnoremap <silent> X "_x
nnoremap <silent> \X "_X

nnoremap <silent> \s "_s
vnoremap <silent> S "_s
nnoremap <silent> \S "_S

nnoremap <silent> \c "_c
vnoremap <silent> C "_c
nnoremap <silent> \C "_C

" Paste with capital P doesn't replace the yank buffer
vnoremap <silent> P "_dP

" Quickly change TAB width
nmap <leader>M :set tabstop=4 shiftwidth=4 softtabstop=4<CR>
nmap <leader>m :set tabstop=2 shiftwidth=2 softtabstop=2<CR>

" Quick print(f"")
command Print :normal oprint(f"")h

" Quick pprint import
command Pprint :normal ofrom pprint import pprint

" Quick ipdb trace
command Ipdb :normal oimport ipdb; ipdb.set_trace()if '_exit' in locals(): exit()pass

" Quick console.log
command Console :normal oconsole.log();h
