set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
let g:python_host_prog = '/home/rik/.asdf/shims/python2'
let g:python3_host_prog = '/home/rik/.asdf/shims/python'
set termguicolors
source ~/.vimrc
