__all__ = (
    'configure',
)


def configure(repl):

    repl.vi_mode = False

    # The `default` colorscheme isn't great on dark backgrounds. The `native`
    # colorscheme is better. Ptpython uses Pygments for highlighting the code,
    # so check the Pygments docs for available color schemes:
    # https://pygments.org/demo/
    repl.use_code_colorscheme('native')
