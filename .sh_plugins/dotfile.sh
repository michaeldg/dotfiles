dotfile() {
    # Fuzzy find a dotfile and edit it
    ls -ap1 ~/ | grep "^\.\w" | fzf | awk '{ print $1 }' | xargs -I % $EDITOR ~/%
}
