if [[ $(command -v broot) ]] ; then

    if [ -n "$ZSH_VERSION" ]; then
        SHELL=zsh
    elif [ -n "$BASH_VERSION" ]; then
        SHELL=bash
    fi

    LAUNCHER_DIR=$HOME/.config/broot/launcher/$SHELL
    LAUNCHER_BIN=$LAUNCHER_DIR/br

    if [[ ! -d "$LAUNCHER_DIR" ]]; then
        mkdir -p $LAUNCHER_DIR
        broot --print-shell-function $SHELL >> $LAUNCHER_BIN
    fi

    source $LAUNCHER_BIN

fi
