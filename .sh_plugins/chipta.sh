alias mut='./manage.py update_translations'
alias muti='./manage.py update_translations -i'

# Open a GitLab MR to branch `master` assignee `@gitaarik` and reviewer `@michaeldg`
alias gmrmm='gmrm 158100'

# Open a GitLab MR to branch `master` assignee `@gitaarik` and reviewer `@owen`
alias gmrmo='gmrm 4913627'

# Docker management commands:
alias dm='./docker/manage.sh'
alias dmsh='./docker/manage.sh shell_plus'
alias dmrs='./docker/manage.sh runserver_plus'
alias dmrss='./docker/manage.sh runserver_plus --cert /tmp/localhost'
alias dmdb='./docker/manage.sh dbshell'
alias dmsm='./docker/manage.sh showmigrations'
alias dmmm='./docker/manage.sh makemigrations'
alias dmm='./docker/manage.sh migrate'
alias dmut='./docker/manage.sh update_translations'
alias dmuti='./docker/manage.sh update_translations -i'

# Django test command where you can give the path to the test file. It will
# convert it to the dotted format.
dmtf() {
    ./docker/manage.sh test --parallel 4 $(echo "$*" | sed 's/\//./g' | sed 's/\.py//g' | sed 's/\.$//')
}

alias dmtfk='dmtf --keepdb'
