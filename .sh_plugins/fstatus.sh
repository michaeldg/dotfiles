# Search for a service and then display the status of that service
fstatus() {
    services=$(sudo service --status-all | sed 1d)
    service_name=$(echo $services | fzf | awk '{ print $4 }')
    service $service_name status
}
