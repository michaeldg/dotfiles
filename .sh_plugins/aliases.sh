alias sa='alias | grep'
alias l='ls -l'
alias la='ls -la'
alias lt='ls --tree'
alias c='cd'
alias c.='cd ..'
alias c-='cd -'
alias v='nvim .'
alias vi='nvim'
alias vip='nvim -p'
alias vs='vi -S Session.vim'
alias j='jobs'
alias f='fg'
alias t='tmux'
alias tmc='tmux loadb -'
alias tmp='tmux saveb -'
alias s='screen'
alias rm='rm -i'
alias grepi='grep -i'
alias rgi='rg -i'
alias rgl='rg -l'
alias si='sudo -i'
alias rst='reset'
alias tree='tree -F'
alias d='docker'
alias dc='docker-compose'
alias clrswp='find . -name "*.swp" -delete'
alias py='python'
alias prjson='python -m json.tool'
alias pt='ptpython'
alias ip='ptipython'

# Fuzzy find file in current dir
alias ff='l | fzf'

# Fuzzy search dir, then CD to it
alias cf='cd $(ls -1d */ | fzf)'

# Fuzzy kill, using fzf :)
alias fkill="ps -ef | sed 1d | fzf | awk '{ print \$2}' | xargs kill"
alias fk=fkill

# Fuzzy search fzf aliasses!
alias sf="alias | grep fzf | fzf | sed 's/^\([^=]*\)=.*$/\1/'"

# YouTube audio download
alias youtube-dl-audio='youtube-dl --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format mp3'
