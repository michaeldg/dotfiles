#!/bin/bash

# Remove all docker images, containers and volumes
dockerdestroy() {

    printf "\n"
    docker system df
    printf "\nAre you sure you want to PERMANENTLY remove all the items listed here? (y/n)\n"

    if [[ -n "$ZSH_VERSION" ]]; then
        read -q "choice?"
    elif [[ -n "$BASH_VERSION" ]]; then
        read -e -p "" choice
    fi

    if [[ "$choice" == [Yy] ]]; then
        docker container ls -a | tail -n +2 | awk '{ print $1 }' | xargs docker container rm
        docker image ls -a | tail -n +2 | awk '{ print $3 }' | xargs docker image rm
        docker volume ls | tail -n +2 | awk '{ print $2 }' | xargs docker volume rm
        docker system prune --force
        docker system df
        printf "\nDone!\n"
    else
        printf "\nAborted\n"
    fi

}
