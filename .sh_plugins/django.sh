#!/bin/sh

# managemand command aliases
alias m='./manage.py'
alias msh='./manage.py shell_plus'
alias mrs='./manage.py runserver_plus'
alias mrss='./manage.py runserver_plus --cert /tmp/localhost'
alias mdb='./manage.py dbshell'
alias msm='./manage.py showmigrations'
alias mmm='./manage.py makemigrations'
alias mm='./manage.py migrate'

# Django test command where you can give the path to the test file. It will
# convert it to the dotted format.
mtf() {
    ./manage.py test --parallel 4 $(echo "$*" | sed 's/\//./g' | sed 's/\.py//g' | sed 's/\.$//')
}

# Django test command where you can give a unit test header.
#
# When a test fails, you see the header of the unit test in the terminal
# output, in a format like this:
#
#     ERROR: test_my_func (my_project.tests.my_test.MyTestCase)
#
# You can feed this string to `mtu` to test this unit test:
#
#     mtu "ERROR: test_my_func (my_project.tests.my_test.MyTestCase)"
#
# And it will run:
#
#     ./manage.py test --parallel 4 my_project.tests.my_test.MyTestCase.test_my_func
#
# So you can just copy the output from the terminal to `mtu` and it will run
# that test for you.
mtu() {
    ./manage.py test --parallel 4 $(echo "$*" | sed 's/^ERROR: //' | awk '{ print $2,".",$1 }' | sed 's/[() ]//g')
}

alias mtfk='mtf --keepdb'
alias mtuk='mtu --keepdb'

# ripgrep search excluding `*/tests/*` directories containing unit tests.
alias rget='rg -g "!*/tests/*"'

# Kill Django's runserver (in case the shell prompt hangs for some reason)
alias dks="ps aux | grep runserver | grep python | head -1 | awk '{ print \$2 }' | xargs kill"

django_recreate_last_migration() {

    app=$1

    if [ -z $app ] ; then
        echo "Please provide an app name"
        return
    fi

    get_migrations() {
        ls -1 ${app}/migrations | grep '^\([0-9][0-9][0-9][0-9]_.*\).py' | sed 's/\.py//'
    }

    last_2_migrations=`get_migrations | tail -2`
    prev_migration=`echo $last_2_migrations | head -1`
    last_migration=`echo $last_2_migrations | tail -1`

    ./manage.py migrate $app $prev_migration
    rm -f ${app}/migrations/${last_migration}.py
    ./manage.py makemigrations $app
    new_migration=`get_migrations | tail -1`
    mv ${app}/migrations/{${new_migration},${last_migration}}.py
    ./manage.py migrate $app

}
