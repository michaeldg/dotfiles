gmr() {
    # A quick way to open a GitLab merge request URL for the current git branch
    # you're on.
    # The optional first argument is the target branch.
    # The optional second argument is the assignee.

    repo_path=$(git remote get-url origin --push | sed 's/^.*://g' | sed 's/.git$//g')
    current_branch=$(git rev-parse --abbrev-ref HEAD)

    if [[ -n $1 ]]; then
        target_branch="&merge_request[target_branch]=$1"
    else
        target_branch=""
    fi

    if [[ -n $2 ]]; then
        reviewer="&merge_request[reviewer_ids][]=$2"
    else
        reviewer=
    fi

    URL="https://gitlab.com/$repo_path/merge_requests/new?merge_request[source_branch]=$current_branch$target_branch$assignee$reviewer&merge_request[force_remove_source_branch]=true"

    if [[ $(command -v xdg-open) ]] ; then
        xdg-open $URL
    else
        echo $URL
    fi

}

# Aliases for common branches
alias gmm='gm master'
alias grbm='grb master'
alias gdim='gd master'
alias gdims='gd master --stat'
alias gcom='gco master'
alias gmrm='gmr master'

alias grbs='grb staging'
alias gdis='gd staging'
alias gcos='gco staging'
alias gmrs='gmr staging'

# Show a diff betwheen the local branch and the upstream
alias gdu='git diff $(git rev-parse --abbrev-ref $(git_current_branch)@{upstream})'

# Fuzzy search for a branch and then check it out
alias gcof='git branch | fzf | xargs git checkout'
