# ZSH loads FZF through a oh-my-zsh plugin
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/fzf

if [ -n "$BASH_VERSION" ]; then
    [ -f ~/.fzf.bash ] && source ~/.fzf.bash
fi
