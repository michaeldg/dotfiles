if [[ -x "$(command -v bit)"  ]] ; then

    if [ -n "$ZSH_VERSION" ]; then
        autoload -U +X bashcompinit && bashcompinit
        complete -o nospace -C /usr/local/bin/bit bit
    elif [ -n "$BASH_VERSION" ]; then
        complete -C /usr/local/bin/bit bit
    fi

fi
