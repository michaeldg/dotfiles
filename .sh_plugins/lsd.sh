# LSD (LSDeluxe) - The next gen ls command 
# https://github.com/Peltoche/lsd
if [[ $(command -v lsd) ]] ; then
    alias ls=lsd
fi
