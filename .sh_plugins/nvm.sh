# Deprecated in favor of asdf

# ZSH loads nvm through a oh-my-zsh plugin, so only do this for Bash
#if [ -n "$BASH_VERSION" ]; then
#    export NVM_DIR="$HOME/.nvm"
#    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
#fi
