# ZSH loads asdf through a oh-my-zsh plugin
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/asdf

if [ -n "$BASH_VERSION" ]; then
    [ -f ~/.asdf/asdf.sh ] && source ~/.asdf/asdf.sh
    [ -f ~/.asdf/completions/asdf.bash ] && source ~/.asdf/completions/asdf.bash
fi
