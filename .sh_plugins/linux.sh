# Linux specific settings
if [[ $OSTYPE == linux* ]]; then

    # This sets the LS_COLORS env var, this will make commands like `tree`
    # display colors.
    eval $(dircolors)

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias igrep='grep -i --color=auto'

fi
