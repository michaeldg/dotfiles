if [ -e $HOME/.z.sh/z.sh ]; then

    . $HOME/.z.sh/z.sh

    # Fuzzy find a recent dir and cd to it
    alias zf="cd \$(z | awk '{ print \$2 }' | fzf --tac)"

fi
