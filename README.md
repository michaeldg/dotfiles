# Requirements

### Arch:

    sudo pacman -Syy

    # base packages
    sudo pacman -S base base-devel linux-firmware

    # tools
    sudo pacman -S reflector rsync zsh cmake curl wget whois fd bat lsd git ripgrep neovim tmux fzf jq xclip httpie pwgen tree

    # virtualbox
    # https://wiki.archlinux.org/title/VirtualBox#Installation_steps_for_Arch_Linux_hosts
    sudo pacman -S virtualbox virtualbox-host-modules-arch

    # Desktop apps:
    sudo apt install -y vlc

### Ubuntu:

    # Get system up-to-date
    sudo apt-get update
    sudo apt-get upgrade -y

    # General dev tools
    sudo apt-get install -y ubuntu-dev-tools

    # Some extra dev tools
    sudo apt install -y cmake autoconf automake jq xclip httpie pwgen tree fzf whois fd-find

    # To install `bat` and `ripgrep`, we need a specific fix for Ubuntu 20.04 LTS:
    # https://askubuntu.com/a/1300824
    sudo apt install -o Dpkg::Options::="--force-overwrite" bat ripgrep

    # Zsh, oh-my-zsh
    sudo apt install -y zsh 

    # Neovim PPA, for latest Neovim version
    sudo add-apt-repository -y ppa:neovim-ppa/stable
    sudo apt-get update
    sudo apt-get install -y neovim python3-neovim

    # Desktop apps:
    sudo apt install -y firefox thunderbird libreoffice gnome-tweaks telegram-desktop

    # Peek, simple GIF screen recorder
    sudo add-apt-repository -y ppa:peek-developers/stable
    sudo apt update
    sudo apt install peek

    # Clementine - Music player
    # https://www.clementine-player.org/downloads
    sudo add-apt-repository ppa:me-davidsansome/clementine
    sudo apt-get update
    sudo apt-get install clementine

## Distribution independent installs

    # Use Zsh as default shell
    chsh -s $(which zsh)

    # Oh My Zsh
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    # Powerlevel10k
    # https://github.com/romkatv/powerlevel10k
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

    # fzf-tab
    # https://github.com/Aloxaf/fzf-tab
    git clone https://github.com/Aloxaf/fzf-tab ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-tab

    # zsh-syntax-highlighting
    # https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md#oh-my-zsh=
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

    # Neovim PPA, for latest Neovim version
    sudo add-apt-repository -y ppa:neovim-ppa/stable
    sudo apt-get update
    sudo apt-get install -y neovim python3-neovim

    mkdir -p ~/dev/

    # Universal ctags:
    git clone https://github.com/universal-ctags/ctags.git ~/dev/ctags
    cd ~/dev/ctags
    ./autogen.sh
    ./configure
    make
    sudo make install
    cd -


    # asdf-vm - for installing multiple versions of all kinds of dev tools
    # https://github.com/asdf-vm/asdf

    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.0
    source ~/.asdf/asdf.sh

    asdf plugin-add python
    asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git

    asdf install python 2.7.18
    asdf install python 3.10.4
    asdf global python 3.10.4 2.7.18

    asdf install nodejs lts


    # Pipenv
    pip install --user pipenv


    # Make sure the ~/.local/bin/ dirs exist
    mkdir -p ~/.local/bin/

    # Add `~/.local/bin/` folder to PATH for now. Will later be added by shell
    # init script.
    export PATH=$HOME/.local/bin:$PATH


    # Tmux Plugin Manager
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm


    # diff-so-fancy, for fancy git-diffs
    git clone https://github.com/so-fancy/diff-so-fancy.git ~/.local/diff-so-fancy
    ln -s ~/.local/diff-so-fancy/diff-so-fancy ~/.local/bin/diff-so-fancy


### Manually install these (if you want):

- [lsd](https://github.com/Peltoche/lsd#installation) - improved `ls` 
- [Nerd fonts](https://www.nerdfonts.com/font-downloads) - For devicons and powerline/airline characters.
- [watchman](https://facebook.github.io/watchman/docs/install.html) - For vim plugin [coc-tsserver](https://github.com/neoclide/coc-tsserver)
- [Mattermost](https://mattermost.com/download/) - Teams chat
- [TLPUI](https://github.com/d4nj1/TLPUI) - Laptop battery optimizations


## [Kitty](https://sw.kovidgoyal.net/kitty/binary.html)

    # Kitty install/update:

    curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

    # Kitty desktop integration:

    # Create a symbolic link to add kitty to PATH (assuming ~/.local/bin is in
    # your PATH)
    ln -s ~/.local/kitty.app/bin/kitty ~/.local/bin/
    # Place the kitty.desktop file somewhere it can be found by the OS
    cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
    # If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
    cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
    # Update the path to the kitty icon in the kitty.desktop file(s)
    sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop

    # You might need to make the `Exec` and `TryExec` also absolute paths:
    # https://github.com/kovidgoyal/kitty/issues/4938
    sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop


- Kitty and nerd fonts: https://erwin.co/kitty-and-nerd-fonts/


## Setup GUI scaling:

https://wiki.archlinux.org/title/HiDPI


## GNOME setup

- GNOME Shell
- GNOME Tweaks
- GNOME Extensions
- Dconf editor

```sh
sudo pacman -S gnome-shell gnome-tweaks gnome-shell-extensions dconf
```

### Extensions

- [Vertical overview](https://github.com/RensAlthuis/vertical-overview)
- [Night Theme Switcher](https://nightthemeswitcher.romainvigier.fr/)


### Configs

```bash
gsettings set org.gnome.desktop.interface clock-format '24h'
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.wm.keybindings toggle-on-all-workspaces []
gsettings set org.gnome.desktop.wm.preferences num-workspaces 4
gsettings set org.gnome.mutter dynamic-workspaces false
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
gsettings set org.gnome.settings-daemon.plugins.power idle-brightness 120

# Enable fractional scaling
# https://wiki.archlinux.org/title/HiDPI#Wayland
gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"

# Use workspaces on all monitors
# http://gregcor.com/2011/05/07/fix-dual-monitors-in-gnome-3-aka-my-workspaces-are-broken/
gsettings set org.gnome.shell.overrides workspaces-only-on-primary false

# Show apps of current workspace only in app switcher
# https://askubuntu.com/questions/464946/force-alt-tab-to-switch-only-on-current-workspace-in-gnome-shell
gsettings set org.gnome.shell.app-switcher current-workspace-only true

# Show minimize and maximize buttons
# https://askubuntu.com/questions/125765/how-do-i-add-minimize-maximize-buttons-to-gnome-shell-windows
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

# Always show bluetooth indicator in menu
# https://askubuntu.com/questions/1203854/ubuntu-18-04-bluetooth-settings-missing-from-upper-right-corner-menu
gsettings set org.gnome.shell had-bluetooth-devices-setup true

# Hide trash icon on desktop
# http://tipsonubuntu.com/2017/10/21/quick-tip-remove-trash-icon-ubuntu-17-10-desktop/
gsettings set org.gnome.nautilus.desktop trash-icon-visible false

# Disable application switching keybindings, because they could intefere
# with the hotkeys to switch workspaces
# https://askubuntu.com/questions/968103/disable-the-app-key-supernum-default-functionality-in-ubuntu-17-10
gsettings set org.gnome.shell.keybindings switch-to-application-1 []
gsettings set org.gnome.shell.keybindings switch-to-application-2 []
gsettings set org.gnome.shell.keybindings switch-to-application-3 []
gsettings set org.gnome.shell.keybindings switch-to-application-4 []
gsettings set org.gnome.shell.keybindings switch-to-application-5 []
gsettings set org.gnome.shell.keybindings switch-to-application-6 []
gsettings set org.gnome.shell.keybindings switch-to-application-7 []
gsettings set org.gnome.shell.keybindings switch-to-application-8 []
gsettings set org.gnome.shell.keybindings switch-to-application-9 []

# Enable extensions
gsettings set org.gnome.shell enabled-extensions ['nightthemeswitcher@romainvigier.fr', 'apps-menu@gnome-shell-extensions.gcampax.github.com', 'places-menu@gnome-shell-extensions.gcampax.github.com', 'drive-menu@gnome-shell-extensions.gcampax.github.com', 'window-list@gnome-shell-extensions.gcampax.github.com', 'windowsNavigator@gnome-shell-extensions.gcampax.github.com', 'vertical-overview@RensAlthuis.github.com']
```

### keyboard shortcuts

To import:

    ./gnome-keybindings.pl -i gnome-keybindings.csv

To export:

    ./gnome-keybindings.pl -e gnome-keybindings.csv

More info: https://askubuntu.com/a/217310/230971


## Thunderbird general settings

Copy the `user.js` file in the `thunderbird/` folder to your Thunderbird
profile folder. To find your profile folder:

- In Thunderbird open the menu and hover the "Help" option
- In the sub menu, click "Trouble shooting information"
- At the bottom of the "Application Basics" table, click about:profiles
- Your profile folder is the Root Directory

For more info see: https://superuser.com/a/1418569/121441

There's also the `prefs.js` file, which should be reviewed before overwriting,
and a backup of the previous `prefs.js` should be made before trying this out.


## Battery power notifications

https://gitlab.com/gitaarik/battery-health-notifications

    cd ~/.local/
    wget https://gitlab.com/gitaarik/battery-health-notifications/-/raw/master/battery_health_notifications.sh
    crontab -l > mycrontab
    echo "*/5 * * * * /bin/bash $HOME/.local/battery_health_notifications.sh" >> mycrontab
    crontab mycrontab
    rm -f mycrontab


## DevDocs config

Config for [devdocs.io](https://devdocs.io/). Import this file on the [settings
page](https://devdocs.io/settings).

[Download](https://gitlab.com/gitaarik/dotfiles/-/blob/master/devdocs-config.json)


## Firefox page source default font size

Put this in `userContent.css`:

    @-moz-document url-prefix(view-source:) {
        #viewsource {
            font-size: 18px;
        }
    }

The `userContent.css` file should be placed in the `chrome/` directory inside
the profile directory, which can be found like this:

Menu -> Help -> More Troubleshooting Information -> Profile Directory

More info:
http://forums.mozillazine.org/viewtopic.php?t=1069325
http://kb.mozillazine.org/UserContent.css


### Docker

```sh
# Docker packages
sudo pacman -S docker docker-compose
```

#### Adding user to `docker` group

To not have to use `sudo` for every `docker` command, you can add your user to
the `docker` group:

```sh
sudo gpasswd -a $(whoami) docker
```

But this basically gives your user unrestricted `root` permissions, which can
be a security issue.

However, it is the easiest and most reliable and supported way of using the
`docker` command without `sudo`. And using `sudo` all the time is a bit
annoying, and is sometimes not convenient for build scripts.

To minimize the root permission exposure time, you can periodically remove all
users from the `docker` group. This command does that:

```sh
getent group docker | cut -d ':' -f4 | sed '/^$/d' | xargs -I {} gpasswd -d {} docker
```

Then a Docker user just needs to add itself to the `docker` group so now and
then, using `sudo`, which is at least password protected.


#### Rootless mode

```sh
# Docker rootless mode
#
# With rootless mode, you can run the docker daemon as a user, without needing
# root permissions. This is more secure and convenient.
#
# https://docs.docker.com/engine/security/rootless/#install

# Shut down the docker service as root in case it's running
sudo systemctl disable --now docker.service docker.socket

# Install recommended packages
# https://docs.docker.com/engine/security/rootless/#distribution-specific-hint
# https://docs.docker.com/engine/security/rootless/#networking-errors
sudo pacman -S fuse-overlayfs slirp4netns

# Configure /etc/subuid & /etc/subgid
# https://docs.docker.com/engine/security/rootless/#prerequisites
# https://unix.stackexchange.com/a/397168/39618

# First delete any possible already-present entries, so that we don't make
# duplicates:
sudo sed -i "/^$(whoami):/d" /etc/subuid /etc/subgid

# Now add the entries:
echo "$(whoami):231072:65536" | sudo tee -a /etc/subuid /etc/subgid

# Install rootless mode
# https://docs.docker.com/engine/security/rootless/#install

# Run docker rootless install script
curl -fsSL https://get.docker.com/rootless | sh

# Now we can enable and start the docker daemon without root
systemctl --user enable --now docker
```


#### Completely remove all Docker stuff

To re-setup Docker completely. Might resolve some weird issues sometimes.

```sh
sudo pacman -R docker docker-compose
sudo rm -rf ~/.docker
sudo rm -rf ~/.local/share/docker
sudo rm -rf ~/.config/docker
sudo rm -rf /etc/docker
sudo rm -rf /var/run/docker
sudo rm -rf /var/lib/docker
```
