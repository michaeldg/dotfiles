# Set the correct locale
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Make the prompt blue, bold and display the current path
__color_bold_blue='\[$(tput bold)\]\[$(tput setaf 4)\]'
__color_white='\[$(tput sgr0)\]'
export PS1="$__color_bold_blue\w \\$ $__color_white"

# Use a big command history
export HISTSIZE=1000
export HISTFILESIZE=2000

# Set the default editor to vim
export EDITOR=nvim
export VISUAL=nvim

# Don't put duplicate lines or lines starting with space in the history
export HISTCONTROL=ignoredups

# Enable globstar behavior, makes **/* match subdirs
shopt -s globstar

# Append to the history file, don't overwrite it
shopt -s histappend

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Auto-correct misspelled file/dirnames
shopt -s cdspell
shopt -s dirspell

# Disable the bell
set bell-style none

# Disable the Ctrl+S freeze binding
# https://stackoverflow.com/a/25391867/1248175
[[ $- == *i* ]] && stty -ixon

# Init bash completion
if [ -f /usr/local/etc/bash_completion ]; then
    . /usr/local/etc/bash_completion
elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Load shell plugins from the ~/.sh_plugins/ dir
if [ -d $HOME/.sh_plugins ]; then

    for f in $HOME/.sh_plugins/*
    do
        source $f
    done

fi
