#!/bin/bash

open_firefox() {
    # force-kill all firefox related processes by id and remove lock file (causes issues on restart)
    PIDS=`ps aux | grep firefox | awk '{ print $2 }'`
    for PID in $PIDS
    do
        kill -9 $PID
    done

    profile=`ls ~/.mozilla/firefox/ | grep 'default-release'`
    lockfile="${HOME}/.mozilla/firefox/${profile}/lock"
    rm -f $lockfile

    # open new Firefox window + tabs
    (exec /usr/bin/firefox \
        -new-window https://www.tradingview.com/chart/USaPtuCM/?symbol=CNSUSDT \
        -new-window https://www.tradingview.com/chart/USaPtuCM/?symbol=BTCUSDT \
        -new-window https://duckduckgo.com/ \
        -new-window -url https://docs.google.com/spreadsheets/d/1VGW_CskfXIGCqwsD7kCnFw284-Adjz5JkXyMuHHbr1U/ \
        -new-tab -url https://docs.google.com/spreadsheets/d/1yJq4_TKgjm2xmaswOVEUlgMJVgzGGMW3o6wcIYiaDus/ \
        -new-tab -url https://web.whatsapp.com/ \
        -new-tab -url https://track.toggl.com/timer/ \
        -new-tab -url https://calendar.protonmail.com/u/0/month \
        -new-tab -url https://www.youtube.com/ \
    &> /dev/null &)
}

move_windows_to_workspace() {

    # top workspace
    wmctrl -r "CNSUSDT"             -t 0
    wmctrl -r "BTCUSDT"             -t 0

    sleep 1

    # middle workspace
    wmctrl -r "Telegram"            -t 1
    wmctrl -r "Signal"              -t 1
    wmctrl -r "Evolution"           -t 1
    wmctrl -r "planning"            -t 1

    sleep 1

    # bottom workspace
    wmctrl -r "Mattermost"          -t 2
    wmctrl -r "CherryTree"          -t 2
    wmctrl -r "DuckDuckGo"          -t 2
    wmctrl -r "repositories/chipta" -t 2

}

position_windows() {

    # top workspace
    wmctrl -r "CNSUSDT"             -e "0,    1,    0, 1920, 1080"
    wmctrl -r "coingecko"           -e "0, 1921,    0, 1920, 1080"

    # middle workspace
    wmctrl -r "Telegram"            -e "0, 1921,    0, 1920, 1080"
    wmctrl -r "Evolution"           -e "0, 1921, 1182, 1600,  836"
    wmctrl -r "planning"            -e "0,    1,    0, 1920, 1080"

    # bottom workspace
    wmctrl -r "Mattermost"          -e "0, 1921, 1182, 1600,  836"
    wmctrl -r "CherryTree"          -e "0, 1921, 1182, 1600,  836"
    wmctrl -r "DuckDuckGo"          -e "0, 1921,    0, 1920, 1080"
    wmctrl -r "repositories/chipta" -e "0,    1,    0, 1920, 1080"
}

(exec cherrytree ~/Documents/repositories/notes-and-documentation/cherrytree/notes.ctb &> /dev/null &)
(exec evolution &> /dev/null &)
(exec signal-desktop &> /dev/null &)
(exec /opt/Telegram/Telegram &> /dev/null &)
open_firefox
(exec /snap/bin/mattermost-desktop &> /dev/null &)
gnome-terminal --tab --command="bash -c 'cd ~/Documents/repositories/chipta; $SHELL'"

sleep 40

# position windows with wmctrl (if installed)
pkg="wmctrl"
dpkg -s $pkg &> /dev/null
if [ $? -eq 0 ]; then
    move_windows_to_workspace
    sleep 5
    position_windows
else
    echo "manually install $pkg package to automatically position all windows: sudo apt-get install $pkg"
fi
