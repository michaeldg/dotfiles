user_pref("accessibility.typeaheadfind.enablesound", false);
user_pref("general.autoScroll", true);
user_pref("mail.biff.play_sound", false);
user_pref("mail.biff.show_alert", false);
user_pref("mail.server.default.check_all_folders_for_new", true);
user_pref("mailnews.default_sort_order", 2);
